<?php

class Utils {

    public static function find_value($data, $ref){

        $ref = explode(".", $ref);
        $_data = $data;
        foreach ($ref as $_i=>$_ref) {
            // if (array_key_exists($_ref, $_data))
            // if ((isset($_data->$_ref)) or (isset($_data->{$_ref})))
            if (!property_exists($_data, $_ref))
                $_data = $_data->$_ref;
            else return NULL;
        }

        return $_data;

    }

    public static function json_check(){

        switch (json_last_error()) {
            case JSON_ERROR_NONE:
                return TRUE;
            break;

            case JSON_ERROR_DEPTH:
            case JSON_ERROR_STATE_MISMATCH:
            case JSON_ERROR_CTRL_CHAR:
            case JSON_ERROR_SYNTAX:
            case JSON_ERROR_UTF8:
                return FALSE;
            break;

            default:
                return FALSE;
            break;

        }

    }

    public static function get_file($file_path){

        if (!self::check_file($file_path))
            return NULL;

        if (!function_exists('file_get_contents'))
            return NULL;

        return file_get_contents($file_path);

    }

    public static function get_file_as_html_object($file_path){

        if (!self::check_file($file_path))
            return NULL;

        if (!function_exists('file_get_html'))
            return NULL;

        return file_get_html($file_path);

    }

    public static function check_file($file_path){

        if (!file_exists($file_path))
            return False;

        return True;

    }

}
