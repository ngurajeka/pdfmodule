<?php

class JsonParser {

    public static function decode_file($filepath=NULL){

        if ($filepath==NULL) return NULL;

        if (!file_exists($filepath)) return NULL;

        $data = json_decode(file_get_contents($filepath));
        if (!Utils::json_check()) return NULL;

        return $data;

    }

    public static function validate_data_on_schema($data, $schema){

        // check if the data or the schema was null
        if ((is_null($data)) or (is_null($schema)))
            return False;

        // if the data was not object and schema was not an array
        if ((!is_object($data)) or (!is_array($schema)))
            return False;

        // foreach ($data as $field=>$_data){
        //     // if doesn't exist on schema
        //     if (!array_key_exists($field, $schema))
        //         return False;

        //     // checking data
        //     // ...
        //     $_type = $schema[$field];
        //     if (!property_exists($_data, $_type))
        //         return False;
        // }

        // check per field
        foreach ($schema as $field => $_type) {
            // check schema is exist on the data
            if (!property_exists($data, $field))
                return False;

            // check if the data has the right data type
            if (!property_exists($data->{$field}, $_type))
                return False;
        }

        return True;

    }

    public static function validate_schemapdf_on_data($globalschema, $schemapdf, $data){

        // check if the schema pdf or the data was null
        if ((is_null($schemapdf)) or (is_null($data)))
            return False;

        // check if the data was an object or the schema was an array
        if ((!is_object($data)) or (!is_array($schemapdf)))
            return False;

        // check rows
        var_dump(self::validate_rows_data($globalschema, $schemapdf['rows'], $data));
        // var_dump($schemapdf);

        return True;

    }

    public static function validate_rows_data($globalschema, $rows, $data){

        // check if the rows was null, or the data was null
        if ((is_null($rows)) or (is_null($data)))
            return False;

        // check if the rows was an object
        if ((!is_array($rows)) or (!is_object($data)))
            return False;

        // check per field
        foreach ($rows as $row){
            $_fieldtype = explode(".", $row['type']['ref'])[2];
            $_globaltype = $globalschema[$_fieldtype];
            $_fielddata = explode(".", $row['data']['ref'])[1];
            // check if the field data exist
            if (!property_exists($data, $_fielddata))
                return False;

            // check if the data was right
            if (!property_exists($data->{$_fielddata}, $_globaltype))
                return False;
        }

        return True;

    }

    public static function parse_row_data($json_data, $row){

        // fetch text / label of the row
        $text = self::find_value($json_data, $row->text->ref);
        // echo $text . PHP_EOL;

        // fetch type of the row
        $type = self::find_value($json_data, $row->type->ref);
        // echo "Type : " . $type . PHP_EOL;

        // check text width.
        // if exist, use it. otherwise set to auto
        $text_width = (isset($row->text->width)) ? $row->text->width : 'auto';
        // echo "Text Width : " . $text_width . PHP_EOL;

        // check content and fetch content
        $content = (isset($row->content)) ? self::find_value($json_data, $row->content->ref) : NULL;
        $content_type = gettype($content);

        // check content and fetch data
        $data = self::find_value($json_data, $row->data->ref);
        $data_type = gettype($data);

        // processing the data based on the type
        switch ($type) {
            case 'datetime':
                if (is_string($data)) echo $text . " : " . $data;
                break;

            case 'radio':
                echo $text . " : " . PHP_EOL;
                switch ($content_type) {
                    case 'object':
                    case 'array':
                        $i = 0;
                        foreach ($content as $key => $value){
                            if (($data) and ($data == $key)) echo "x";
                            echo "\t" . $value;
                            if (($i+1)<sizeof((array) $content)) echo PHP_EOL;
                            $i++;
                        }
                        break;

                    default:
                        echo "Wrong Content Type";
                        break;
                }
                break;

            case 'list':
                echo $text . " : " . PHP_EOL;
                switch ($content_type) {
                    case 'object':
                    case 'array':
                        $i=0;
                        foreach ($content as $key => $value){
                            if (isset($data->$key)) echo $data->$key;
                            echo "\t" . $value;
                            if (($i+1)<sizeof((array) $content)) echo PHP_EOL;
                            $i++;
                        }
                        break;
                    case 'NULL':
                        if (is_array($data) or is_object($data)){
                            $i=0;
                            foreach ($data as $key => $value){
                                echo "\t" . $value;
                                if (($i+1)<sizeof((array) $data)) echo PHP_EOL;
                                $i++;
                            }
                        }
                        break;
                    default:
                        echo "Wrong Content Type";
                        break;
                }
                break;

            case 'string':
            case 'int':
                echo $text . " : ";
                if (is_string($data) or is_int($data)) echo $data;
                break;

            default:
                echo $text . " : " . $type;
                break;
        }

    }

    public static function parse_column_data($json_data, $column_data){

        echo "-----------------------------------------------" . PHP_EOL;
        echo "Title: " . $column_data->title . PHP_EOL;
        echo "Layout: " . $column_data->layout . PHP_EOL;
        echo "Rows: " . PHP_EOL;
        // iterate rows
        foreach ($column_data->rows as $rows) {
            foreach ($rows as $i=>$row) {
                $row_data = self::find_value($json_data, $row->ref);
                if ($row_data) self::parse_row_data($json_data, $row_data);
                if ((sizeof($rows)>1) and (($i+1)<sizeof($rows))) echo ", ";
            }
            echo PHP_EOL;
        }
        echo "-----------------------------------------------" . PHP_EOL;

    }

    public static function parse_page_data($json_data){

        $pages = $json_data->metadata->extension->pdf->pages;

        // need to fix this iteration
        foreach ($pages as $page) {
            // extracting content
            foreach ($page->content as $columns) {
                foreach ($columns as $column) {
                    $_column = self::find_value($json_data, $column->ref);
                    self::parse_column_data($json_data, $_column);
                }
            }
        }

    }

}
