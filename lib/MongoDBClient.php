<?php

class MongoDBClient {

    protected $connection;
    protected $connected;
    protected $error;
    protected $collection;
    protected $db;
    protected $data;

    public function __construct(){

        try {
            $this->connection = new MongoClient();
            // prodigy is a name of the database on MongoDB
            $this->db = $this->connection->prodigy;
            $this->connected = True;
        } catch (MongoConnectionException $e) {
            $this->error = $e->getMessage();
            $this->connected = False;
        }

    }

    public function is_connected(){
        return $this->connected;
    }

    public function get_error(){
        return $this->error;
    }

    public function set_collection($collection_name){

        if ($collection_name==NULL) return False;

        $this->collection = $this->db->selectCollection($collection_name);
        return True;

    }

    public function get_document($id){

        if ((is_null($id)) or ($id===""))
            return NULL;

        $documents = $this->collection->findOne(array("_id" => $id));

        return $documents;

    }

    public function get_schema($id){

        if ((is_null($id)) or ($id===""))
            return NULL;

        $documents = $this->get_document($id);
        if (!isset($documents[$id]))
            return NULL;

        $schema = [];
        foreach ($documents[$id] as $doc)
            foreach ($doc as $key => $value)
                $schema[$key] = $value;

        return $schema;

    }

    public function get_document_data($id){

        if ((is_null($id)) or ($id===""))
            return NULL;

        $documents = $this->get_document($id);

        return $documents[$id];

    }

}
