<?php

function auto_loader($class_name){
    include_once __DIR__ . '/' . $class_name .'.php';
}

spl_autoload_register('auto_loader');
