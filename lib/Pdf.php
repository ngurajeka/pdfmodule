<?php

class Pdf {

    protected $pdf;
    protected $css;
    protected $output;

    public function __construct() {
        $this->pdf = new mPDF('c','A4','','dejavusanscondensed',0,0,0,0,10,10);
    }

    public function set_css($filepath){
        if (file_exists($filepath))
            $this->css = file_get_contents($filepath);
    }

    public function write_header($html){
        if (!is_null($html))
            $this->pdf->SetHTMLHeader($html);
    }

    public function write_footer($html){
        if (!is_null($html))
            $this->pdf->SetHTMLFooter($html);
    }

    public function write_output(){
        if (!is_null($this->css)){
            $this->pdf->WriteHTML($this->css, 1);
            $this->pdf->WriteHTML($this->output, 2);
        } else $this->pdf->WriteHTML($this->output);
    }

    public function get_pdf_file(){
        return $this->pdf->Output();
    }

    public function set_output($output){
        $this->output = $output;
    }

    public function get_output(){
        return $this->output;
    }

}
