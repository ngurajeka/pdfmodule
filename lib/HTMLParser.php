<?php

class HTMLParser {

    protected $twig;
    protected $schemapdf;
    protected $globalschema;
    protected $data;

    public function __construct($twig, $schemapdf, $globalschema, $data){
        $this->twig = $twig;
        $this->schemapdf = $schemapdf;
        $this->globalschema = $globalschema;
        $this->data = $data;
    }

    public function parse_column($field_column){

        $list_column = $this->schemapdf['columns'];
        $column = $list_column[$field_column];

        // add validation here

        $title = strtoupper($column['title']['en']);
        $layout = $column['layout'];
        $list_rows = [];

        foreach ($column['rows'] as $list) {
            $_list = [];
            $_list['color'] = (isset($list['color'])) ? $list['color'] : NULL;
            foreach ($list['content'] as $row) {
                $field_row = explode(".", $row['ref'])[3];
                $_list['field'][$field_row] = $this->parse_row($field_row);
            }
            $list_rows[] = $_list;
        }

        $html = $this->twig->render('column/'.$layout.'.html',
                                    [
                                        'title' => $title,
                                        'list_rows' => $list_rows
                                    ]);

        return $html;

    }

    public function parse_row($field_row){

        $list_rows = $this->schemapdf['rows'];
        $row = $list_rows[$field_row];
        $row_data = $this->data->{$field_row};

        // type, label, label width, layout, data
        $row_type = explode(".", $row['type']['ref'])[2];
        $row_type = $this->globalschema[$row_type];
        $label = $row_data->label;
        $label_width = (isset($row['text']['width'])) ? "width:".$row['text']['width']."%;" : NULL;
        $layout = $row['layout'];
        $data = $row_data->{$row_type};

        return [
            'label' => $label,
            'label_width' => $label_width,
            'row_type' => $row_type,
            'layout' => $layout,
            'data' => $data
        ];

    }

}
