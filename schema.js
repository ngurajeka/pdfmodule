{
    "metadata": {
        "generic": {
            "form_code": "string",
            "form_date": "datetime",
            "form_title": "string",
            "office_id": "string",
            "listing_date": "datetime",
            "listing_expired": "datetime",
            "membership_number": "int",
            "ma_name": "string",
            "phone_number": "string",
            "office_name": "string",
            "bo_name": "string",
            "telephone": "string",
            "fax": "string",
            "office_email": "string",
            "commission": "int",
            "property_owner_name": "string",
            "property_owner_address": "string",
            "property_owner_city": "string",
            "property_owner_post_code": "int",
            "property_owner_districts": "string",
            "property_owner_province": "string",
            "property_owner_phone_number": "string",
            "property_owner_fax": "string",
            "property_owner_telephone": "string",
            "property_owner_email": "string",
            "property_owner_ktp_number": "string",
            "property_owner_marriage_status": "string",
            "property_address": "string",
            "property_rt": "string",
            "property_rw": "string",
            "property_blok": "string",
            "property_house_number": "string",
            "property_district": "string",
            "property_districts": "string",
            "property_city": "string",
            "property_province": "string",
            "property_post_code": "int",
            "property_price": "int",
            "property_status": "radio",
            "property_listing_category": "radio",
            "property_type": "radio",
            "property_land_length": "int",
            "property_land_width": "int",
            "property_land_size": "int",
            "property_building_length": "int",
            "property_building_width": "int",
            "property_building_size": "int",
            "property_building_floors": "int",
            "property_facility": "list",
            "owner_status": "radio",
            "notes": "string",
            "images": "list"
        },
        "backend": {
            "form_date": {
                "label": "Tanggal",
                "format": "Y-m-d H:i:s"
            },
            "form_title": {
                "label": "Title"
            },
            "office_id": {
                "label": "Office ID"
            },
            "listing_date": {
                "label": "Tanggal Listing",
                "format": "Y-m-d H:i:s"
            },
            "listing_expired": {
                "label": "Tanggal Kadaluarsa",
                "format": "Y-m-d H:i:s"
            },
            "membership_number": {
                "label": "Membership Number"
            },
            "ma_name": {
                "label": "Nama MA"
            },
            "phone_number": {
                "label": "No. Handphone"
            },
            "office_name": {
                "label": "Nama Kantor"
            },
            "bo_name": {
                "label": "Nama BO"
            },
            "telephone": {
                "label": "Telepon"
            },
            "fax": {
                "label": "Fax"
            },
            "office_email": {
                "label": "Email Kantor"
            },
            "commission": {
                "label": "Komisi",
                "suffix": "%"
            },
            "property_owner_name": {
                "label": "Nama Pemilik Properti"
            },
            "property_owner_address": {
                "label": "Alamat Pemilik Properti"
            },
            "property_owner_city": {
                "label": "Kota"
            },
            "property_owner_post_code": {
                "label": "Kode Pos"
            },
            "property_owner_districts": {
                "label": "Kecamatan"
            },
            "property_owner_province": {
                "label": "Provinsi"
            },
            "property_owner_phone_number": {
                "label": "No. Handphone"
            },
            "property_owner_fax": {
                "label": "Fax"
            },
            "property_owner_telephone": {
                "label": "Telepon"
            },
            "property_owner_email": {
                "label": "Email"
            },
            "property_owner_ktp_number": {
                "label": "No. KTP"
            },
            "property_owner_marriage_status": {
                "label": "Status Pernikahan"
            },
            "property_address": {
                "label": "Alamat Properti"
            },
            "property_rt": {
                "label": "RT"
            },
            "property_rw": {
                "label": "RW"
            },
            "property_blok": {
                "label": "Blok"
            },
            "property_house_number": {
                "label": "No. Rumah"
            },
            "property_district": {
                "label": "Kabupaten"
            },
            "property_districts": {
                "label": "Kecamatan"
            },
            "property_city": {
                "label": "Kota"
            },
            "property_province": {
                "label": "Provinsi"
            },
            "property_post_code": {
                "label": "Kode Pos"
            },
            "property_price": {
                "label": "Harga Permintaan Properti",
                "suffix": {
                    "type": "radio",
                    "radio": {
                        "1": "IDR",
                        "2": "USD"
                    }
                }
            },
            "property_status": {
                "label": "Status Listing",
                "radio": {
                    "1": "Primary",
                    "2": "Secondary"
                }
            },
            "property_listing_category": {
                "label": "Kategori Listing",
                "radio": {
                    "1": "Dijual",
                    "2": "Disewakan",
                    "3": "Lelang",
                    "4": "Kerja Sama",
                    "5": "Tukar Menukar"
                }
            },
            "property_type": {
                "label": "Jenis Properti",
                "radio": {
                    "1": "Rumah Tinggal",
                    "2": "Ruko",
                    "3": "Apartemen",
                    "4": "Vila/Resort",
                    "5": "Gudang",
                    "6": "Kantor",
                    "7": "Pabrik",
                    "8": "Lahan",
                    "9": "Kavling",
                    "10": "Condotel",
                    "11": "Kios",
                    "12": "Lainnya"
                }
            },
            "property_land_length": {
                "label": "Panjang Tanah",
                "suffix": "m"
            },
            "property_land_width": {
                "label": "Lebar Tanah",
                "suffix": "m"
            },
            "property_land_size": {
                "label": "Luas Tanah",
                "suffix": "m2"
            },
            "property_building_length": {
                "label": "Panjang Bangunan",
                "suffix": "m"
            },
            "property_building_width": {
                "label": "Lebar Bangunan",
                "suffix": "m"
            },
            "property_building_size": {
                "label": "Luas Bangunan",
                "suffix": "m2"
            },
            "property_building_floors": {
                "label": "Jumlah Lantai"
            },
            "property_facility": {
                "label": "Fasilitas",
                "list": {
                    "1": "Kamar Tidur Utama",
                    "2": "Kamar Tidur",
                    "3": "Kamar Tidur Pembantu",
                    "4": "Jumlah Kamar Tidur",
                    "5": "Kamar Mandi Utama",
                    "6": "Kamar Mandi",
                    "7": "Kamar Mandi Pembantu",
                    "8": "Jumlah Kamar Mandi",
                    "9": "Gudang",
                    "10": "Garasi",
                    "11": "Taman",
                    "12": "Hadap",
                    "13": "Lantai",
                    "14": "Listrik",
                    "15": "Air",
                    "16": "Telepon",
                    "17": "Jumlah AC",
                    "18": "Lainnya"
                }
            },
            "owner_status": {
                "label": "Status Kepemilikan",
                "radio": {
                    "1": "Sertifikat Hak Milik",
                    "2": "Hak Guna Bangunan",
                    "3": "Hak Pakai",
                    "4": "Hak Guna Usaha",
                    "5": "SHM Sarusun (Strata Title)"
                }
            },
            "notes": {
                "label": "Keterangan Tambahan"
            },
            "images": {
                "label": "Image"
            }
        },
        "extension": {
            "pdf": {
                "rows": {
                    "1": {
                        "type": { "ref": "metadata.generic.listing_date" },
                        "text": {
                            "ref": "metadata.backend.listing_date.label",
                            "width": 12
                        },
                        "data": { "ref": "data.listing_date" },
                        "layout": "row_1"
                    },
                    "2": {
                        "type": { "ref": "metadata.generic.listing_expired" },
                        "text": {
                            "ref": "metadata.backend.listing_expired.label",
                            "width": 12
                        },
                        "data": { "ref": "data.listing_expired" },
                        "layout": "row_1"
                    },
                    "3": {
                        "type": { "ref": "metadata.generic.membership_number" },
                        "text": {
                            "ref": "metadata.backend.membership_number.label",
                            "width": 12
                        },
                        "data": { "ref": "data.membership_number" },
                        "layout": "row_1"
                    },
                    "4": {
                        "type": { "ref": "metadata.generic.ma_name" },
                        "text": {
                            "ref": "metadata.backend.ma_name.label",
                            "width": 12
                        },
                        "data": { "ref": "data.ma_name" },
                        "layout": "row_1"
                    },
                    "5": {
                        "type": { "ref": "metadata.generic.phone_number" },
                        "text": {
                            "ref": "metadata.backend.phone_number.label",
                            "width": 12
                        },
                        "data": { "ref": "data.phone_number" },
                        "layout": "row_1"
                    },
                    "6": {
                        "type": { "ref": "metadata.generic.office_name" },
                        "text": {
                            "ref": "metadata.backend.office_name.label",
                            "width": 12
                        },
                        "data": { "ref": "data.office_name" },
                        "layout": "row_1"
                    },
                    "7": {
                        "type": { "ref": "metadata.generic.bo_name" },
                        "text": {
                            "ref": "metadata.backend.bo_name.label",
                            "width": 12
                        },
                        "data": { "ref": "data.bo_name" },
                        "layout": "row_1"
                    },
                    "8": {
                        "type": { "ref": "metadata.generic.telephone" },
                        "text": {
                            "ref": "metadata.backend.telephone.label",
                            "width": 12
                        },
                        "data": { "ref": "data.telephone" },
                        "layout": "row_1"
                    },
                    "9": {
                        "type": { "ref": "metadata.generic.fax" },
                        "text": {
                            "ref": "metadata.backend.fax.label",
                            "width": 12
                        },
                        "data": { "ref": "data.fax" },
                        "layout": "row_1"
                    },
                    "10": {
                        "type": { "ref": "metadata.generic.office_email" },
                        "text": {
                            "ref": "metadata.backend.office_email.label",
                            "width": 12
                        },
                        "data": { "ref": "data.office_email" },
                        "layout": "row_1"
                    },
                    "11": {
                        "type": { "ref": "metadata.generic.commission" },
                        "text": {
                            "ref": "metadata.backend.commission.label",
                            "width": 12
                        },
                        "data": { "ref": "data.commission" },
                        "layout": "row_1"
                    },
                    "12": {
                        "type": { "ref": "metadata.generic.property_owner_name" },
                        "text": {
                            "ref": "metadata.backend.property_owner_name.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_name" },
                        "layout": "row_1"
                    },
                    "13": {
                        "type": { "ref": "metadata.generic.property_owner_address" },
                        "text": {
                            "ref": "metadata.backend.property_owner_address.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_address" },
                        "layout": "row_1"
                    },
                    "14": {
                        "type": { "ref": "metadata.generic.property_owner_city" },
                        "text": {
                            "ref": "metadata.backend.property_owner_city.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_city" },
                        "layout": "row_1"
                    },
                    "15": {
                        "type": { "ref": "metadata.generic.property_owner_post_code" },
                        "text": {
                            "ref": "metadata.backend.property_owner_post_code.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_post_code" },
                        "layout": "row_1"
                    },
                    "16": {
                        "type": { "ref": "metadata.generic.property_owner_districts" },
                        "text": {
                            "ref": "metadata.backend.property_owner_districts.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_districts" },
                        "layout": "row_1"
                    },
                    "17": {
                        "type": { "ref": "metadata.generic.property_owner_province" },
                        "text": {
                            "ref": "metadata.backend.property_owner_province.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_province" },
                        "layout": "row_1"
                    },
                    "18": {
                        "type": { "ref": "metadata.generic.property_owner_phone_number" },
                        "text": {
                            "ref": "metadata.backend.property_owner_phone_number.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_phone_number" },
                        "layout": "row_1"
                    },
                    "19": {
                        "type": { "ref": "metadata.generic.property_owner_fax" },
                        "text": {
                            "ref": "metadata.backend.property_owner_fax.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_fax" },
                        "layout": "row_1"
                    },
                    "20": {
                        "type": { "ref": "metadata.generic.property_owner_telephone" },
                        "text": {
                            "ref": "metadata.backend.property_owner_telephone.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_telephone" },
                        "layout": "row_1"
                    },
                    "21": {
                        "type": { "ref": "metadata.generic.property_owner_email" },
                        "text": {
                            "ref": "metadata.backend.property_owner_email.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_email" },
                        "layout": "row_1"
                    },
                    "22": {
                        "type": { "ref": "metadata.generic.property_owner_ktp_number" },
                        "text": {
                            "ref": "metadata.backend.property_owner_ktp_number.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_ktp_number" },
                        "layout": "row_1"
                    },
                    "23": {
                        "type": { "ref": "metadata.generic.property_owner_marriage_status" },
                        "text": {
                            "ref": "metadata.backend.property_owner_marriage_status.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_owner_marriage_status" },
                        "layout": "row_1"
                    },
                    "24": {
                        "type": { "ref": "metadata.generic.property_address" },
                        "text": {
                            "ref": "metadata.backend.property_address.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_address" },
                        "layout": "row_1"
                    },
                    "25": {
                        "type": { "ref": "metadata.generic.property_rt" },
                        "text": {
                            "ref": "metadata.backend.property_rt.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_rt" },
                        "layout": "row_1"
                    },
                    "26": {
                        "type": { "ref": "metadata.generic.property_rw" },
                        "text": {
                            "ref": "metadata.backend.property_rw.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_rw" },
                        "layout": "row_1"
                    },
                    "27": {
                        "type": { "ref": "metadata.generic.property_blok" },
                        "text": {
                            "ref": "metadata.backend.property_blok.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_blok" },
                        "layout": "row_1"
                    },
                    "28": {
                        "type": { "ref": "metadata.generic.property_house_number" },
                        "text": {
                            "ref": "metadata.backend.property_house_number.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_house_number" },
                        "layout": "row_1"
                    },
                    "29": {
                        "type": { "ref": "metadata.generic.property_district" },
                        "text": {
                            "ref": "metadata.backend.property_district.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_district" },
                        "layout": "row_1"
                    },
                    "30": {
                        "type": { "ref": "metadata.generic.property_districts" },
                        "text": {
                            "ref": "metadata.backend.property_districts.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_districts" },
                        "layout": "row_1"
                    },
                    "31": {
                        "type": { "ref": "metadata.generic.property_city" },
                        "text": {
                            "ref": "metadata.backend.property_city.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_city" },
                        "layout": "row_1"
                    },
                    "32": {
                        "type": { "ref": "metadata.generic.property_province" },
                        "text": {
                            "ref": "metadata.backend.property_province.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_province" },
                        "layout": "row_1"
                    },
                    "33": {
                        "type": { "ref": "metadata.generic.property_post_code" },
                        "text": {
                            "ref": "metadata.backend.property_post_code.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_post_code" },
                        "layout": "row_1"
                    },
                    "34": {
                        "type": { "ref": "metadata.generic.property_price" },
                        "text": {
                            "ref": "metadata.backend.property_price.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_price.data" },
                        "layout": "row_1"
                    },
                    "35": {
                        "type": { "ref": "metadata.generic.property_status" },
                        "text": {
                            "ref": "metadata.backend.property_status.label",
                            "width": 12
                        },
                        "content": { "ref": "metadata.backend.property_status.radio" },
                        "data": { "ref": "data.property_status" },
                        "layout": "row_2"
                    },
                    "36": {
                        "type": { "ref": "metadata.generic.property_listing_category" },
                        "text": {
                            "ref": "metadata.backend.property_listing_category.label",
                            "width": 12
                        },
                        "content": { "ref": "metadata.backend.property_listing_category.radio" },
                        "data": { "ref": "data.property_listing_category" },
                        "layout": "row_2"
                    },
                    "37": {
                        "type": { "ref": "metadata.generic.property_type" },
                        "text": {
                            "ref": "metadata.backend.property_type.label",
                            "width": 12
                        },
                        "content": { "ref": "metadata.backend.property_type.radio" },
                        "data": {
                            "ref": "data.property_type",
                            "max_radio": 4
                        },
                        "layout": "row_2"
                    },
                    "38": {
                        "type": { "ref": "metadata.generic.property_land_length" },
                        "text": {
                            "ref": "metadata.backend.property_land_length.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_land_length" },
                        "layout": "row_1"
                    },
                    "39": {
                        "type": { "ref": "metadata.generic.property_land_width" },
                        "text": {
                            "ref": "metadata.backend.property_land_width.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_land_width" },
                        "layout": "row_1"
                    },
                    "40": {
                        "type": { "ref": "metadata.generic.property_land_size" },
                        "text": {
                            "ref": "metadata.backend.property_land_size.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_land_size" },
                        "layout": "row_1"
                    },
                    "41": {
                        "type": { "ref": "metadata.generic.property_building_length" },
                        "text": {
                            "ref": "metadata.backend.property_building_length.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_building_length" },
                        "layout": "row_1"
                    },
                    "42": {
                        "type": { "ref": "metadata.generic.property_building_width" },
                        "text": {
                            "ref": "metadata.backend.property_building_width.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_building_width" },
                        "layout": "row_1"
                    },
                    "43": {
                        "type": { "ref": "metadata.generic.property_building_size" },
                        "text": {
                            "ref": "metadata.backend.property_building_size.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_building_size" },
                        "layout": "row_1"
                    },
                    "44": {
                        "type": { "ref": "metadata.generic.property_building_floors" },
                        "text": {
                            "ref": "metadata.backend.property_building_floors.label",
                            "width": 12
                        },
                        "data": { "ref": "data.property_building_floors" },
                        "layout": "row_1"
                    },
                    "45": {
                        "type": { "ref": "metadata.generic.property_facility" },
                        "text": {
                            "ref": "metadata.backend.property_facility.label",
                            "width": 12,
                            "style": "bold"
                        },
                        "content": { "ref": "metadata.backend.property_facility.list" },
                        "data": {
                            "ref": "data.property_facility",
                            "max_list": 4
                        },
                        "layout": "row_3"
                    },
                    "46": {
                        "type": { "ref": "metadata.generic.owner_status" },
                        "text": {
                            "ref": "metadata.backend.owner_status.label",
                            "width": 12
                        },
                        "content": { "ref": "metadata.backend.owner_status.radio" },
                        "data": { "ref": "data.owner_status" },
                        "layout": "row_2"
                    },
                    "47": {
                        "type": { "ref": "metadata.generic.notes" },
                        "text": {
                            "ref": "metadata.backend.notes.label",
                            "width": 12
                        },
                        "data": { "ref": "data.notes" },
                        "layout": "row_1"
                    },
                    "48": {
                        "type": { "ref": "metadata.generic.images" },
                        "text": {
                            "ref": "metadata.backend.images.label",
                            "width": 12
                        },
                        "content": { "ref": "metadata.backend.images.list" },
                        "data": {
                            "ref": "data.images",
                            "max_list": 5
                        },
                        "layout": "row_4"
                    }
                },
                "columns": {
                    "1": {
                        "title": "",
                        "layout": "column_1",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.1" } ],
                            "2": [ { "ref": "metadata.extension.pdf.rows.2" } ],
                            "3": [ { "ref": "metadata.extension.pdf.rows.3" } ],
                            "4": [ { "ref": "metadata.extension.pdf.rows.4" } ],
                            "5": [ { "ref": "metadata.extension.pdf.rows.5" } ],
                            "6": [ { "ref": "metadata.extension.pdf.rows.6" } ],
                            "7": [ { "ref": "metadata.extension.pdf.rows.7" } ],
                            "8": [ { "ref": "metadata.extension.pdf.rows.8" } ],
                            "9": [ { "ref": "metadata.extension.pdf.rows.9" } ],
                            "10": [ { "ref": "metadata.extension.pdf.rows.10" } ]
                        }
                    },
                    "2": {
                        "title": "",
                        "layout": "column_1",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.12" } ],
                            "2": [ { "ref": "metadata.extension.pdf.rows.13" } ],
                            "3": [ { "ref": "metadata.extension.pdf.rows.14" }, { "ref": "metadata.extension.pdf.rows.15" } ],
                            "4": [ { "ref": "metadata.extension.pdf.rows.16" } ],
                            "5": [ { "ref": "metadata.extension.pdf.rows.17" } ],
                            "6": [ { "ref": "metadata.extension.pdf.rows.18" }, { "ref": "metadata.extension.pdf.rows.19" } ],
                            "7": [ { "ref": "metadata.extension.pdf.rows.20" } ],
                            "8": [ { "ref": "metadata.extension.pdf.rows.21" } ],
                            "9": [ { "ref": "metadata.extension.pdf.rows.22" } ],
                            "10": [ { "ref": "metadata.extension.pdf.rows.23" } ]
                        }
                    },
                    "3": {
                        "title": "DATA PROPERTI",
                        "layout": "column_2",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.24" } ],
                            "2": [ { "ref": "metadata.extension.pdf.rows.25" }, { "ref": "metadata.extension.pdf.rows.26" }, { "ref": "metadata.extension.pdf.rows.27" }, { "ref": "metadata.extension.pdf.rows.28" } ],
                            "3": [ { "ref": "metadata.extension.pdf.rows.29" } ],
                            "4": [ { "ref": "metadata.extension.pdf.rows.30" } ],
                            "5": [ { "ref": "metadata.extension.pdf.rows.31" } ],
                            "6": [ { "ref": "metadata.extension.pdf.rows.32" } ],
                            "7": [ { "ref": "metadata.extension.pdf.rows.33" } ],
                            "8": [ { "ref": "metadata.extension.pdf.rows.34" } ],
                            "9": [ { "ref": "metadata.extension.pdf.rows.35" } ],
                            "10": [ { "ref": "metadata.extension.pdf.rows.36" } ],
                            "11": [ { "ref": "metadata.extension.pdf.rows.37" } ],
                            "12": [ { "ref": "metadata.extension.pdf.rows.38" }, { "ref": "metadata.extension.pdf.rows.39" }, { "ref": "metadata.extension.pdf.rows.40" } ],
                            "13": [ { "ref": "metadata.extension.pdf.rows.41" }, { "ref": "metadata.extension.pdf.rows.42" }, { "ref": "metadata.extension.pdf.rows.43" }, { "ref": "metadata.extension.pdf.rows.44" } ],
                            "14": [ { "ref": "metadata.extension.pdf.rows.45" } ]
                        }
                    },
                    "4": {
                        "title": "ASPEK LEGAL",
                        "layout": "column_2",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.46" } ]
                        }
                    },
                    "5": {
                        "title": "ASPEK LEGAL",
                        "layout": "column_1",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.47" } ]
                        }
                    },
                    "6": {
                        "title": "FOTO",
                        "layout": "column_1",
                        "rows": {
                            "1": [ { "ref": "metadata.extension.pdf.rows.48" } ]
                        }
                    }
                },
                "pages": {
                    "1": {
                        "content": {
                            "1": [ { "ref": "metadata.extension.pdf.columns.1" }, { "ref": "metadata.extension.pdf.columns.2" } ],
                            "2": [ { "ref": "metadata.extension.pdf.columns.3" } ],
                            "3": [ { "ref": "metadata.extension.pdf.columns.4" } ],
                            "4": [ { "ref": "metadata.extension.pdf.columns.5" } ],
                            "5": [ { "ref": "metadata.extension.pdf.columns.6" } ]
                        }
                    }
                }
            }
        }
    },
    "data": {
        "form_code": "A-F.2",
        "form_date": "2015-09-20 15:30:01",
        "form_title": "FORM LISTING PROPERTI",
        "office_id": "LX-1234",
        "listing_date": "2015-09-20 15:30:01",
        "listing_expired": "2015-12-20 15:30:01",
        "membership_number": 286,
        "ma_name": "Agent XXX",
        "phone_number": "628991342518",
        "office_name": "Office Tes",
        "bo_name": "Pemilik",
        "telephone": "-",
        "fax": "-",
        "office_email": "office@mail.com",
        "commission": 10,
        "property_owner_name": "Yang Punya",
        "property_owner_address": "Jl. XXX",
        "property_owner_city": "Jakarta Selatan",
        "property_owner_post_code": 9220,
        "property_owner_districts": "Setiabudi",
        "property_owner_province": "DKI Jakarta",
        "property_owner_phone_number": "628991342518",
        "property_owner_fax": "-",
        "property_owner_telephone": "-",
        "property_owner_email": "ganteng@mail.com",
        "property_owner_ktp_number": "025345287989",
        "property_owner_marriage_status": "Menikah",
        "property_address": "Jl. Komando",
        "property_rt": "I",
        "property_rw": "II",
        "property_blok": "A",
        "property_house_number": "25",
        "property_district": "-",
        "property_districts": "Setiabudi",
        "property_city": "Jakarta Selatan",
        "property_province": "DKI Jakarta",
        "property_post_code": 92220,
        "property_price": {
            "data": 150000000,
            "suffix": 1
        },
        "property_status": 1,
        "property_listing_category": 1,
        "property_type": 1,
        "property_land_length": 120,
        "property_land_width": 120,
        "property_land_size": 120,
        "property_building_length": 120,
        "property_building_width": 120,
        "property_building_size": 120,
        "property_building_floors": 4,
        "property_facility": {
            "1": 2,
            "2": 2,
            "18": 1
        },
        "notes": "-",
        "images": {
            "1": "/path/to/image",
            "2": "/path/to/image",
            "3": "/path/to/image",
            "4": "/path/to/image",
            "5": "/path/to/image",
            "6": "/path/to/image"
        }
    }
}
