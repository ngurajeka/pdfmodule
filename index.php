<?php

include_once __DIR__ . '/lib/autoload.php';
include_once __DIR__ . '/vendor/autoload.php';

function action(){

    $mongo = new MongoDBClient();
    if (!$mongo->is_connected())
        return $mongo->get_error() . PHP_EOL;

    $mongo->set_collection("metadata.global");
    $schema = $mongo->get_schema("af_1");
    $mongo->set_collection("filetype.pdf");
    $schemapdf = $mongo->get_document_data("af_1");
    $data = JsonParser::decode_file('json/data.json');

    $loader = new Twig_Loader_Filesystem(__DIR__ . '/templates');
    $twig = new Twig_Environment($loader, array());
    $htmlparser = new HTMLParser($twig, $schemapdf, $schema, $data);
    $output = NULL;
    foreach($schemapdf['columns'] as $field_name => $column)
        $output .= $htmlparser->parse_column($field_name);

    $pdf = new Pdf();
    $pdf->set_css(__DIR__ . '/static/css/modulepdf.css');

    // set header
    $header_data = [
        'header' => [
            'logo' => 'static/img/remax_logo.png',
            'form_code' => [
                'label' => $data->form_code->label,
                'data' => $data->form_code->string
            ],
            'form_date' => [
                'label' => $data->form_date->label,
                'data' => $data->form_date->datetime
            ],
            'form_title' => [
                'data' => $data->form_title->string
            ],
        ]
    ];
    $html_header = $twig->render('form/header.html', $header_data);
    $html_footer = $twig->render('form/footer.html', []);
    $pdf->write_header($html_header);
    $pdf->write_footer($html_footer);
    $pdf->set_output($output);
    $pdf->write_output();

    return $pdf->get_pdf_file();

}
action();
